<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

defined('MOODLE_INTERNAL') || die();

class block_androgogic_catalogue extends block_base {

	function init() {
		$this->title = get_string('plugintitle','block_androgogic_catalogue');
		$this->cron = 1;
	}

	function get_content() {
        
        if ($this->content !== null) {
			return $this->content;
		}
        
		$this->content = new stdClass;
        $url = new moodle_url('/blocks/androgogic_catalogue/index.php');
		$this->content->text = '<a href="'.$url.'">'.get_string('catalogue_entry_search','block_androgogic_catalogue').'</a>';

		return $this->content;
	}

    function has_config() {
        return true;
    }

}
