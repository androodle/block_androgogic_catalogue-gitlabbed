<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

require_once('../../config.php');
require_once('lib.php');
require_once('forms/location_search_form.php');

$delete = optional_param('delete', null, PARAM_INT);

$context = context_system::instance();
$pageurl = new moodle_url('/blocks/androgogic_catalogue/locations.php');

$PAGE->set_context($context);
$PAGE->set_url($pageurl);
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('location_search', 'block_androgogic_catalogue'));
$PAGE->requires->css('/blocks/androgogic_catalogue/css/multi-select.css');
$PAGE->set_heading(get_string('plugintitle', 'block_androgogic_catalogue'));
$PAGE->navbar->add(get_string('catalogue_entry_search','block_androgogic_catalogue'), "$CFG->wwwroot/blocks/androgogic_catalogue/index.php");

if ($delete && has_capability('block/androgogic_catalogue:delete', $context)) {
    $data = new stdClass();
    $data->id = $delete;
    $location = new \block_androgogic_catalogue\location($data);
    try {
        $location->delete();
    } catch (Exception $ex) {
        echo $OUTPUT->notification($ex->getMessage());
    }
    redirect($pageurl, get_string('itemdeleted','block_androgogic_catalogue'));
}

include_once('tabs.php');

echo $OUTPUT->header();
echo $OUTPUT->tabtree($tabs, 'locations');

require_capability('block/androgogic_catalogue:edit', $context);

$mform = new location_search_form();

echo html_writer::start_tag('div', array('class' => 'catalogue-links'));
echo html_writer::tag('a', 
    get_string('location_new', 'block_androgogic_catalogue'),
    array(
        'href' => new moodle_url('location_edit.php'),
        'class' => 'catalogue-button-link link-as-button',
    )
);
echo html_writer::end_tag('div');

$data = $mform->get_data();
if (!$data) {
    // Pass some GET values into the form.
    $data = new stdClass();
}
$mform->set_data($data);
$mform->display();

$results = \block_androgogic_catalogue\locations::search($data);
$result_count = count($results);

if (!$results) {
    echo $OUTPUT->notification(get_string('noresults', 'block_androgogic_catalogue'));
    echo $OUTPUT->footer();
    die;
} 

echo $OUTPUT->notification(get_string('entriesfound', 'block_androgogic_catalogue', $result_count), 'notifysuccess');

$table = new html_table();
$table->head = array (
    get_string('name', 'block_androgogic_catalogue'),
    ''
);
foreach ($results as $result) {
    $links = array();
    if (has_capability('block/androgogic_catalogue:edit', $context)) {
        $links[] = html_writer::tag('a', 
            get_string('edit'),
            array(
                'href' => new moodle_url('location_edit.php', array('id' => $result->id))
            )
        );
    }
    if (has_capability('block/androgogic_catalogue:delete', $context)) {
        $links[] = html_writer::tag('a', 
            get_string('delete'),
            array(
                'href' => new moodle_url('locations.php', array('delete' => $result->id)),
                'onclick' => "return confirm('Are you sure you want to delete this item?');"
            )
        );
    }
    $table->data[] = array (
        $result->name,
        implode(' &middot; ', $links)
    );
}

echo html_writer::table($table);

echo $OUTPUT->footer();
