<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

namespace block_androgogic_catalogue;

class entry
{
    public $description;
    public $id;
    public $name;
    
    public function __construct($data = null) 
    {
        $this->load_data($data);
    }
    
    protected function load_data($data = null) 
    {
        if (empty($data)) {
            return;
        }
        foreach (get_object_vars($data) as $k => $v) {
            $this->{$k} = $v;
        }
    }
    
    public function get_programs() 
    {
        global $DB;
        
        $sql = "SELECT p.* 
            FROM {prog} p 
            LEFT JOIN {androgogic_catalogue_entry_programs} cp ON cp.program_id = p.id 
            LEFT JOIN {androgogic_catalogue_entries} a ON a.id = cp.catalogue_entry_id
            WHERE a.id = ?";
        $results = $DB->get_records_sql($sql, array($this->id));
        if (!$results) {
            return FALSE;
        }
        
        // Add URLs
        foreach ($results as &$result) {
            $result->url = new \moodle_url('/totara/program/view.php', array('id' => $result->id));
        }
        
        return $results;
    }
    
    public function get_courses() 
    {
        global $DB;
        
        $sql = "SELECT c.* 
            FROM {course} c
            LEFT JOIN {androgogic_catalogue_entry_courses} cc ON cc.course_id = c.id
            LEFT JOIN {androgogic_catalogue_entries} a ON a.id = cc.catalogue_entry_id
            WHERE a.id = ?";
        $results = $DB->get_records_sql($sql, array($this->id));
        if (!$results) {
            return FALSE;
        }
        
        // Add URLs
        foreach ($results as &$result) {
            $enrol_methods = $this->get_enrol_methods($result->id);
            $has_self_enrolment = in_array('self', $enrol_methods);
            $has_lp_enrolment = in_array('totara_learningplan', $enrol_methods);

            if ($has_lp_enrolment & !$has_self_enrolment) {
                $result->url = new \moodle_url('add_to_learningplan.php', array('courseid' => $result->id));
            } else {
                $result->url = new \moodle_url('/course/view.php', array('id' => $result->id));
            }
        }
        
        return $results;
    }
    
    public function delete() 
    {
        global $DB;
        
        if (empty($this->id)) {
            throw new \Exception(get_string('idwrong', 'block_androgogic_catalogue'));
        }
        
        $DB->delete_records('androgogic_catalogue_entries', array('id' => $this->id));
        $DB->delete_records('androgogic_catalogue_entry_locations', array('catalogue_entry_id' => $this->id));
        $DB->delete_records('androgogic_catalogue_entry_courses', array('catalogue_entry_id' => $this->id));
        $DB->delete_records('androgogic_catalogue_entry_programs', array('catalogue_entry_id' => $this->id));
        $DB->delete_records('androgogic_catalogue_entry_organisations', array('catalogue_entry_id' => $this->id));
        $DB->delete_records('androgogic_catalogue_entry_positions', array('catalogue_entry_id' => $this->id));
        $DB->delete_records('androgogic_catalogue_entry_competencies', array('catalogue_entry_id' => $this->id));
        $DB->delete_records('androgogic_catalogue_entry_cohorts', array('catalogue_entry_id'=> $this->id));
    }
    
    public function load($id) 
    {
        global $DB;

        $entry = $DB->get_record('androgogic_catalogue_entries', array('id' => $id));
        if (!$entry) {
            throw new Exception('entrynotfound', 'block_androgogic_catalogue');
        }
        
        // Preparing for HTML editor.
        $entry->descriptionformat = 1;

        $entry->location_id = array();
        $result = $DB->get_records('androgogic_catalogue_entry_locations', array('catalogue_entry_id' => $id));
        foreach ($result as $row) {
            $entry->location_id[] = $row->location_id;
        }

        $entry->course_id = array();
        $result = $DB->get_records('androgogic_catalogue_entry_courses', array('catalogue_entry_id' => $id));
        foreach ($result as $row) {
            $entry->course_id[] = $row->course_id;
        }

        $entry->program_id = array();
        $result = $DB->get_records('androgogic_catalogue_entry_programs', array('catalogue_entry_id' => $id));
        foreach ($result as $row) {
            $entry->program_id[] = $row->program_id;
        }

        $entry->organisation_id = array();
        $result = $DB->get_records('androgogic_catalogue_entry_organisations', array('catalogue_entry_id' => $id));
        foreach ($result as $row) {
            $entry->organisation_id[] = $row->organisation_id;
        }

        $entry->position_id = array();
        $result = $DB->get_records('androgogic_catalogue_entry_positions', array('catalogue_entry_id' => $id));
        foreach ($result as $row) {
            $entry->position_id[] = $row->position_id;
        }

        $entry->competency_id = array();
        $result = $DB->get_records('androgogic_catalogue_entry_competencies', array('catalogue_entry_id' => $id));
        foreach ($result as $row) {
            $entry->competency_id[] = $row->competency_id;
        }

        $entry->cohort_id = array();
        $result = $DB->get_records('androgogic_catalogue_entry_cohorts', array('catalogue_entry_id' => $id));
        foreach ($result as $row) {
            $entry->cohort_id[] = $row->cohort_id;
        }

        $this->load_data($entry);
    }

    function save($data) {

        global $USER, $DB;

        $data->modified_by = $USER->id;
        $data->datemodified = time();
        $data->description = $data->description_editor['text'];

        if (!empty($data->id)) {
            $DB->update_record('androgogic_catalogue_entries', $data);
        } else {
            $data->created_by = $USER->id;
            $data->datecreated = time();
            $data->id = $DB->insert_record('androgogic_catalogue_entries',$data);
        }

        $DB->delete_records('androgogic_catalogue_entry_locations', array('catalogue_entry_id' => $data->id));
        if (isset($data->location_id)) {
            foreach ($data->location_id as $location_id) {
                $insert = new \stdClass();
                $insert->catalogue_entry_id = $data->id;
                $insert->location_id = $location_id;
                $DB->insert_record('androgogic_catalogue_entry_locations', $insert);
            }
        }

        $DB->delete_records('androgogic_catalogue_entry_courses', array('catalogue_entry_id' => $data->id));
        if (isset($data->course_id)) {
            foreach ($data->course_id as $course_id) {
                $insert = new \stdClass();
                $insert->catalogue_entry_id = $data->id;
                $insert->course_id = $course_id;
                $DB->insert_record('androgogic_catalogue_entry_courses', $insert);
            }
        }

        $DB->delete_records('androgogic_catalogue_entry_programs', array('catalogue_entry_id' => $data->id));
        if (isset($data->program_id)) {
            foreach ($data->program_id as $program_id) {
                $insert = new \stdClass();
                $insert->catalogue_entry_id = $data->id;
                $insert->program_id = $program_id;
                $DB->insert_record('androgogic_catalogue_entry_programs', $insert);
            }
        }

        $DB->delete_records('androgogic_catalogue_entry_organisations', array('catalogue_entry_id' => $data->id));
        if (isset($data->organisation_id)) {
            foreach ($data->organisation_id as $organisation_id) {
                $insert = new \stdClass();
                $insert->catalogue_entry_id = $data->id;
                $insert->organisation_id = $organisation_id;
                $DB->insert_record('androgogic_catalogue_entry_organisations', $insert);
            }
        }

        $DB->delete_records('androgogic_catalogue_entry_positions', array('catalogue_entry_id' => $data->id));
        if (isset($data->position_id)) {
            foreach ($data->position_id as $position_id) {
                $insert = new \stdClass();
                $insert->catalogue_entry_id = $data->id;
                $insert->position_id = $position_id;
                $DB->insert_record('androgogic_catalogue_entry_positions', $insert);
            }
        }

        $DB->delete_records('androgogic_catalogue_entry_competencies', array('catalogue_entry_id' => $data->id));
        if (isset($data->competency_id)) {
            foreach ($data->competency_id as $competency_id) {
                $insert = new \stdClass();
                $insert->catalogue_entry_id = $data->id;
                $insert->competency_id = $competency_id;
                $DB->insert_record('androgogic_catalogue_entry_competencies', $insert);
            }
        }

        $DB->delete_records('androgogic_catalogue_entry_cohorts', array('catalogue_entry_id' => $data->id));
        if (isset($data->cohort_id)) {
            foreach ($data->cohort_id as $cohort_id) {
                $insert = new \stdClass();
                $insert->catalogue_entry_id = $data->id;
                $insert->cohort_id = $cohort_id;
                $DB->insert_record('androgogic_catalogue_entry_cohorts', $insert);
            }
        }

        return $data->id;
        
    }
    
    public function get_enrol_methods($courseid)
    {
        global $DB;
        return $DB->get_fieldset_select('enrol', 'enrol', 'courseid = ?', array('courseid' => $courseid));
    }
}