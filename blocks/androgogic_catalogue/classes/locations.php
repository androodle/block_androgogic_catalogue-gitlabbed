<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

namespace block_androgogic_catalogue;

class locations
{
    public static function search(&$data) 
    {
        global $DB;
        
        $where = array();
        $params = array();
        if (!empty($data->search)) {
            $where[] = $DB->sql_like('a.name', ':search');
            $params['search'] = '%'.$data->search.'%';
        }
        $sql = "SELECT a.*  
            FROM {androgogic_catalogue_locations} a
            ".(!empty($where) ? ' WHERE '.implode(' AND ', $where) : '')."
            ORDER BY a.name";
        
        return $DB->get_records_sql($sql, $params);
    }
}