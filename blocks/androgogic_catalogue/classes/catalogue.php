<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

namespace block_androgogic_catalogue;

class catalogue
{
    public static function get_view_mode()
    {
        global $CFG, $SESSION;
        
        $view = optional_param('view', '', PARAM_TEXT);
        
        // Remember the way we want to view the calendar.
        // If nothing chosen, set the default mode according to the plugin settings.
        if (!isset($SESSION->catalogue_view_mode)) {
            if (isset($CFG->catalogue_view_mode)) {
                $SESSION->catalogue_view_mode = $CFG->catalogue_view_mode;
            } else {
                $SESSION->catalogue_view_mode = 'standard';
            }
        }
        if (!empty($view)) {
            $SESSION->catalogue_view_mode = $view;
        }
        
        return $SESSION->catalogue_view_mode;
    }
    
    protected function build_search_joins() 
    {
        // We have to keep all these tables here, because there can be
        // elaborate filtering applied in WHERE, based on the search and 
        // visibility settings.
        return "FROM {androgogic_catalogue_entries} a 
            LEFT JOIN {androgogic_catalogue_entry_locations} cel ON a.id = cel.catalogue_entry_id
            LEFT JOIN {androgogic_catalogue_locations} cl ON cel.location_id = cl.id
            LEFT JOIN {androgogic_catalogue_entry_courses} cec ON a.id = cec.catalogue_entry_id
            LEFT JOIN {course} c ON cec.course_id = c.id
            LEFT JOIN {androgogic_catalogue_entry_programs} cep ON a.id = cep.catalogue_entry_id
            LEFT JOIN {prog} p ON cep.program_id = p.id
            LEFT JOIN {androgogic_catalogue_entry_organisations} ceo ON a.id = ceo.catalogue_entry_id
            LEFT JOIN {org} o ON ceo.organisation_id = o.id
            LEFT JOIN {androgogic_catalogue_entry_positions} cepos ON a.id = cepos.catalogue_entry_id
            LEFT JOIN {pos} pos ON cepos.position_id = pos.id
            LEFT JOIN {androgogic_catalogue_entry_competencies} cecomp ON a.id = cecomp.catalogue_entry_id
            LEFT JOIN {comp} comp ON cecomp.competency_id = comp.id
            LEFT JOIN {androgogic_catalogue_entry_cohorts} cecoh ON a.id = cecoh.catalogue_entry_id
            LEFT JOIN {cohort} coh ON cecoh.cohort_id = coh.id";
    }

    protected function build_search_where(&$data) 
    {
        global $USER, $CFG;
        
        $where = array();
        $params = array();
        
        if (!empty($CFG->catalogue_comp_framework)) {
            // Limit results only to a selected framework.
            $where[] = "comp.frameworkid = :frameworkid";
            $params['frameworkid'] = $CFG->catalogue_comp_framework;
        }
        
        // Apply search filters.
        self::apply_search_filters($data, $where, $params);
        
        // If they are not logged in, they don't get to see non-public entries.
        if (empty($USER->id)) {
            $where[] = "(a.public = 1 
                AND o.id IS NULL 
                AND pos.id IS NULL 
                AND coh.id IS NULL)";
        } elseif (!has_capability('block/androgogic_catalogue:edit', \context_system::instance())) {
            // Apply visibility filters (organisation, position, audience),
            // unless user has the right to edit all entries.
            self::apply_visibility_filters($where, $params);
        }
        
        return array($where, $params);
    }
    
    private static function apply_search_filters($data, &$where, &$params)
    {
        global $DB;
        
        if (!empty($data->q)) {
            $where[] = $DB->sql_like($DB->sql_concat('a.name', 'a.description'), ':search', FALSE);
            $params['search'] = '%'.$data->q.'%';
        }
        
        if (!empty($data->l)) {
            $where[] = "cl.id = :locationid";
            $params['locationid'] = $data->l;
        }
        
        if (!empty($data->c)) {
            $where[] = "comp.id = :compid";
            $params['compid'] = $data->c;
        }
        
        if (isset($data->ct) && $data->ct != -1) {
            $where[] = "c.coursetype = :coursetype";
            $params['coursetype'] = $data->ct;
        }
        
        if (!empty($data->ds)) {
            $where[] = "c.startdate >= :startdate1 or p.availablefrom >= :startdate2";
            $params['startdate1'] = $data->ds;
            $params['startdate2'] = $data->ds;
        }
        
        if (!empty($data->de)) {
            $where[] = "p.availablefrom <= :enddate";
            $params['enddate'] = $data->de;
        }

    }
    
    private static function apply_visibility_filters(&$where, &$params)
    {
        global $DB, $USER;
        
        $userdata = self::get_user_data($USER->id);
        $v_where = array();
        
        // Apply the organisation filters.
        if (!empty($userdata->organisations)) {
            $o_sql = '';
            $o_params = array();
            $i = 0;
            foreach ($userdata->organisations as $userorg) {
                $o_param = 'userorg'.++$i;
                // User's organisation path has to contain the entry
                // organisation path in order for the entry to be visible.
                // Forward slashes appended to both values to avoid false positives.
                $o_sql[] = $DB->sql_position($DB->sql_concat('o.path', "'/'"), ':'.$o_param) .' > 0';
                $o_params[$o_param] = $userorg.'/';
            }
            $v_where[] = '('. implode(' OR ', $o_sql) .')';
            $params = array_merge($params, $o_params);
        }
        
        // Apply the position filters.
        if (!empty($userdata->positions)) {
            $p_sql = '';
            $p_params = array();
            $i = 0;
            foreach ($userdata->positions as $userpos) {
                $p_param = 'userpos'.++$i;
                // User's position path has to contain the entry
                // position path in order for the entry to be visible.
                // Forward slashes appended to both values to avoid false positives.
                $p_sql[] = $DB->sql_position($DB->sql_concat('o.path', "'/'"), ':'.$p_param) .' > 0';
                $p_params[$p_param] = $userpos.'/';
            }
            $v_where[] = '('. implode(' OR ', $p_sql) .')';
            $params = array_merge($params, $p_params);
        }
        
        // Apply the audience filters.
        if (!empty($userdata->cohorts)) {
            list($t_sql, $t_params) = $DB->get_in_or_equal($userdata->cohorts, SQL_PARAMS_NAMED, 'usercohortid');
            $v_where[] = '(cecoh.cohort_id '. $t_sql .')';
            $params = array_merge($params, $t_params);
        }
        
        // All these filters must be joined by OR, and not AND
        // (i.e., user is not allowed to see this entry via audience, but
        // via organisation instead).
        // However, if there are no visibility filters set at all for an entry
        // (NULLs in all joined tables), then this filtering is not applicable.
        $where[] = '(
            (ceo.catalogue_entry_id IS NULL 
                AND cepos.catalogue_entry_id IS NULL
                AND cecoh.catalogue_entry_id IS NULL)
            OR ('. implode(' OR ', $v_where) .'))';
    }
    
    public function search_count(&$data) 
    {
        global $DB;
        
        list($where, $params) = $this->build_search_where($data);
        
        $sql = "SELECT COUNT(DISTINCT a.id)
            ".$this->build_search_joins()."";
        if (!empty($where)) {
            $sql .= ' WHERE '.implode(' AND ', $where);
        }
        
        return $DB->get_field_sql($sql, $params);
    }
    
    public function one($id)
    {
        $entry = new entry();
        if (!empty($id)) {
            try {
                $entry->load($id);
            } catch (Exception $ex) {
                return false;
            }
        }
        
        return array($entry);
    }

    public function search(&$data, $from = 0, $limit = 20) 
    {
        global $DB;
        
        list($where, $params) = $this->build_search_where($data);
        
        $sql = "SELECT DISTINCT a.*  
            ".$this->build_search_joins();
        if (!empty($where)) {
            $sql .= ' WHERE '.implode(' AND ', $where);
        }
        $sql .= " ORDER BY a.name ";
        
        if (self::get_view_mode() == 'calendar') {
            // Do not limit the number of entries for the calendar view.
            $from = $limit = null;
        }
        
        return $DB->get_records_sql($sql, $params, $from, $limit);
    }
 
    public static function get_comp_frameworks()
    {
        global $DB;
        return $DB->get_records_menu('comp_framework', NULL, NULL, 'id, fullname');
    }
    
    /**
     * Retrieves all organisations, positions and cohorts this user belongs to.
     * Organisations and positions are returned as arrays of paths,
     * audiences as an array of audience IDs.
     */
    protected static function get_user_data($userid)
    {
        global $DB;

        $data = new \stdClass();
        $data->organisations = array();
        $data->positions = array();
        $data->cohorts = array();
        
        // Get user's organisations.
        $q = "SELECT o.path 
            FROM {pos_assignment} pa
            INNER JOIN {org} o ON pa.organisationid = o.id
            WHERE pa.userid = ? 
                AND pa.type IN (1,2)
            GROUP BY o.path";
        $results = $DB->get_records_sql($q, array($userid));
        if ($results) {
            $data->organisations = array_keys($results);
        }

        // Get user's positions.
        $q = "SELECT p.path 
            FROM {pos_assignment} pa
            INNER JOIN {pos} p ON pa.positionid = p.id
            WHERE pa.userid = ? 
                AND pa.type IN (1,2)
            GROUP BY p.path";
        $results = $DB->get_records_sql($q, array($userid));
        if ($results) {
            $data->positions = array_keys($results);
        }

        // Get user's audiences.
        $q = "SELECT cm.cohortid 
            FROM {cohort_members} cm
            WHERE cm.userid = ?";
        $results = $DB->get_records_sql($q, array($userid));
        if ($results) {
            $data->cohorts = array_keys($results);
        }

        return $data;
    }
    
}
