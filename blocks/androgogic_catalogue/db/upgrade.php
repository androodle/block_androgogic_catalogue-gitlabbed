<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

function xmldb_block_androgogic_catalogue_upgrade($oldversion=0) {
    
    global $DB;
    
    $result = true;
    $dbman = $DB->get_manager();
    
    if ($oldversion < 2015062300) {
        
        $table = new xmldb_table('androgogic_catalogue_entry_cohorts');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('catalogue_entry_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('cohort_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_index('unq_catalogue_entry_cohort', XMLDB_INDEX_UNIQUE, array('catalogue_entry_id', 'cohort_id'));
        
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        $table = new xmldb_table('androgogic_catalogue_entry_competencies');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('catalogue_entry_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('competency_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_index('unq_catalogue_entry_competency', XMLDB_INDEX_UNIQUE, array('catalogue_entry_id', 'competency_id'));
        
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
    }
    
    if ($oldversion < 2015080600) {
        
        // Convert datetimes to integers, because install.xml doesn't support
        // datetimes, because Moodle is very very stupid in this regard.
        // MySQL-only so far.
        
        $table = new xmldb_table('androgogic_catalogue_entries');
        $field = new xmldb_field('enddate', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('datecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('datemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        $sql = "UPDATE {androgogic_catalogue_entries} SET
               enddate = IFNULL(UNIX_TIMESTAMP(end_date), 0),
               datecreated = IFNULL(UNIX_TIMESTAMP(date_created), 0),
               datemodified = IFNULL(UNIX_TIMESTAMP(date_modified), 0)";
        $DB->execute($sql);
        
        $field = new xmldb_field('end_date');
        $dbman->drop_field($table, $field);
        $field = new xmldb_field('date_created');
        $dbman->drop_field($table, $field);
        $field = new xmldb_field('date_modified');
        $dbman->drop_field($table, $field);
        
        $table = new xmldb_table('androgogic_catalogue_locations');
        $field = new xmldb_field('datecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('datemodified', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        $sql = "UPDATE {androgogic_catalogue_locations} SET
               datecreated = IFNULL(UNIX_TIMESTAMP(date_created), 0),
               datemodified = IFNULL(UNIX_TIMESTAMP(date_modified), 0)";
        $DB->execute($sql);
        
        $field = new xmldb_field('date_created');
        $dbman->drop_field($table, $field);
        $field = new xmldb_field('date_modified');
        $dbman->drop_field($table, $field);
    }
    
    return $result;
}

