<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

defined('MOODLE_INTERNAL') || die();

$tabs = array();

$tabs[] = new tabobject(
    'index', 
    new moodle_url('/blocks/androgogic_catalogue/index.php'), 
    get_string('catalogue_entry_search','block_androgogic_catalogue')
);

if (has_capability('block/androgogic_catalogue:edit', $context)){
    $tabs[] = new tabobject(
        'locations', 
        new moodle_url('/blocks/androgogic_catalogue/locations.php'), 
        get_string('location_search','block_androgogic_catalogue')
    );
}
